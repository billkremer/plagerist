# PLAGERIZER

This is a [plagiarizing](https://www.dictionary.com/browse/plagiarize) site.

We take great famous quotes, swap some words and make them yours!


## APIs

* [Quotes on Design](http://quotesondesign.com/wp-json/posts)
* [DataMuse](https://www.datamuse.com/api/)


## Log
* 1-April-2019 creation
* 25-April-2019 getting synonyms for long words.  Click on title and observe in console.

## How To Run Locally

1. git clone this repo to your computer
2. $ npm install
3. $ npm start
4. navigate to [http://localhost:5000](http://localhost:5000)


### Big Plan
take a quote + author, break it up and make the long words dropdowns with synonyms.  picking the dropdowns then sumbit saves to a "link" /6 random letter combo id that saves to db as id, full quote and author and new quote and author and email.  make searchable by id value or email address


<!-- theQuote = {
	
} -->